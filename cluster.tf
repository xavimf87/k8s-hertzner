module "hcloud_kubernetes_cluster" {
  kubernetes_version = var.kubernetes_version
  source          = "./modules/hcloud_kubernetes_cluster"
  cluster_name    = var.cluster_name
  hcloud_token    = var.hcloud_token
  hcloud_ssh_keys = [hcloud_ssh_key.default.id]
  master_type     = var.master_type
  worker_type     = var.worker_type
  worker_count    = var.worker_count
}


