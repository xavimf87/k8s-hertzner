# Kubernetes for Hertzner Cloud

#### Fork project : [JWDobken](https://github.com/JWDobken/terraform-hcloud-kubernetes)

#### Now compatible with versions =< 1.24.3

---
## Environment variable
Set TOKEN HCloud

```
export TF_VAR_hcloud_token=""
```

---
## Terraform:

```cmd
terraform init .
terraform plan -var-file="cluster.tfvars"
terraform apply -var-file="cluster.tfvars"
```

Save Kubeconfig file with:

```
terraform output -raw kubeconfig > cluster-hcloud.conf
```

Get nodes:
```
kubectl get nodes --kubeconfig=cluster-hcloud.conf
```

You can destroy the created resources with:

```cmd
terraform destroy -var-file="cluster.tfvars"
```
---
Load balancer anotations for service Type LoadBalancer
```
annotations:
    load-balancer.hetzner.cloud/name: 1_lb
    load-balancer.hetzner.cloud/location: hel1
    load-balancer.hetzner.cloud/use-private-ip: true
```
