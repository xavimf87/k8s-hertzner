resource "hcloud_ssh_key" "default" {
  name       = "default"
  public_key = file(var.public_ssh_key)
}