output "load_balancer" {
  value = hcloud_load_balancer.load_balancer
}

output "kubeconfig" {
  value = module.hcloud_kubernetes_cluster.kubeconfig
}

output "hcloud_kubernetes_cluster" {
  value = module.hcloud_kubernetes_cluster
}
